package lasalleurl.sallesocialnetwork.Model;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by victo on 27/03/2017.
 */

public class User implements Serializable {
    public String mail;
    public String name;
    public String lastname;
    public String sex;
    public String birthdateString;
    public String age;
    public String studies;
    public boolean isFriend;


    public User(){}

    public User(String mail, String name, String lastname, String sex, String birthdateString, String studies){
        this.mail = mail;
        this.name = name;
        this.lastname = lastname;
        this.sex = sex;
        this.birthdateString = birthdateString;
        Calendar birthdate = Calendar.getInstance();
        birthdate.set(Integer.parseInt(birthdateString.split("/")[2]), Integer.parseInt(birthdateString.split("/")[1]),Integer.parseInt(birthdateString.split("/")[0]));
        Calendar today = Calendar.getInstance();
        int a = today.get(Calendar.YEAR) - birthdate.get(Calendar.YEAR);
        if ((birthdate.get(Calendar.MONTH) > today.get(Calendar.MONTH))
                || (birthdate.get(Calendar.MONTH) == today.get(Calendar.MONTH) && birthdate.get(Calendar.DAY_OF_MONTH) > today
                .get(Calendar.DAY_OF_MONTH))) {
            a--;
        }
        this.age = String.valueOf(a);
        this.studies = studies;
        this.isFriend = false;
    }


    public String toString() {
        return this.name + " " + this.lastname + " " + this.age;
    }

}
