package lasalleurl.sallesocialnetwork.Application;

import android.os.Bundle;
import android.widget.ListView;

import lasalleurl.sallesocialnetwork.Adapters.ListViewAdapter;
import lasalleurl.sallesocialnetwork.R;


public class ListUsers extends BaseActivity {

    public ListView listView;
    public ListViewAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listusers);
        MyApplication myApp = getMyApplication();
        listView = (ListView) findViewById(R.id.listview);


        adapter = new ListViewAdapter(getApplicationContext(),R.layout.user_row, myApp.users);

        listView.setAdapter(adapter);
    }
    public void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
    }
    public void onBackPressed() {
        finish();
    }


}
