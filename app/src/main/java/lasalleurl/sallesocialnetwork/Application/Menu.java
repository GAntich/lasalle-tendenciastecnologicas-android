package lasalleurl.sallesocialnetwork.Application;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import lasalleurl.sallesocialnetwork.Adapters.GridViewAdapter;
import lasalleurl.sallesocialnetwork.R;

public class Menu extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        getSupportActionBar().hide();

        GridView gridview = (GridView) findViewById(R.id.gridview);
        gridview.setAdapter(new GridViewAdapter(this));
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                switch(position){
                    case 0:
                        Intent create = new Intent(getApplicationContext(), CreateUser.class);
                        startActivity(create);

                        break;
                    case 1:
//                        MyApplication myApp = (MyApplication)getApplication();
//                        showToast(myApp.users.get(0).toString());

                        Intent list = new Intent(getApplicationContext(), ListUsers.class);
                        startActivity(list);

                        break;
                    case 2:
                        save();
                        finish();
                        break;
                }
            }
        });
    }
    public void onBackPressed() {
        save();
    }
}
