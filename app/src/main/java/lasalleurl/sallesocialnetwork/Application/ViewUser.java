package lasalleurl.sallesocialnetwork.Application;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import lasalleurl.sallesocialnetwork.R;
import lasalleurl.sallesocialnetwork.Model.User;

public class ViewUser extends BaseActivity {

    public static final String EXTRA_USER = "user";
    public static final String EXTRA_POSITION = "position";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_user);
        final User user = (User)getIntent().getSerializableExtra(EXTRA_USER);
        final int position = (int)getIntent().getSerializableExtra(EXTRA_POSITION);

        TextView nameView = (TextView) findViewById(R.id.nameUser) ;
        TextView surnameView = (TextView) findViewById(R.id.surnameUser) ;
        TextView ageView = (TextView) findViewById(R.id.dob) ;
        TextView studiesView = (TextView) findViewById(R.id.studies) ;
        final TextView isFriend = (TextView) findViewById(R.id.isFriend);
        final Button friendButton = (Button) findViewById(R.id.friendbutton);


        nameView.setText(user.name);
        surnameView.setText(user.lastname);
        ageView.setText(user.birthdateString);
        studiesView.setText(user.studies);
        if(user.isFriend){
            isFriend.setText(R.string.isFriendTrue);
            isFriend.setTextColor(Color.GREEN);
            friendButton.setText(R.string.isFriendButtonRemove);
        }else{
            isFriend.setText(R.string.isFriendFalse);
            isFriend.setTextColor(Color.BLACK);
            friendButton.setText(R.string.isFriendButtonAdd);
        }

        friendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication myapp = getMyApplication();
                if(user.isFriend) {
                   myapp.users.get(position).isFriend = false;
                    user.isFriend = false;
                    isFriend.setText(R.string.isFriendFalse);
                    isFriend.setTextColor(Color.BLACK);
                    friendButton.setText(R.string.isFriendButtonAdd);
                }else{
                    myapp.users.get(position).isFriend = true;
                    user.isFriend = true;
                    isFriend.setText(R.string.isFriendTrue);
                    isFriend.setTextColor(Color.GREEN);
                    friendButton.setText(R.string.isFriendButtonRemove);
                }
            }
        });
    }
    public void onBackPressed() {
        finish();
    }
}
