package lasalleurl.sallesocialnetwork.Application;


import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import lasalleurl.sallesocialnetwork.R;

import static lasalleurl.sallesocialnetwork.R.layout.activity_authentication;

public class Authentication extends BaseActivity {
    private static final int EXIT_INTERVAL_TIME = 3000;
    private static final int MIN_PASSWORD_LENGTH = 6;
    private boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(activity_authentication);
        getSupportActionBar().hide();
        Button loginButton = (Button) findViewById(R.id.loginButton);
        EditText passText = (EditText) findViewById((R.id.passwordTextBox));
        setOnFocusChangeListener(passText);
        setOnClickListener(loginButton);
    }

    public void setOnFocusChangeListener(final EditText editText) {
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) {
                    if(editText.getText().toString().trim().length() < MIN_PASSWORD_LENGTH) {
                        editText.setError(getString(R.string.passwordError));
                    }else {
                        editText.setError(null);
                    }
                }else {
                    if(editText.getText().toString().trim().length() < MIN_PASSWORD_LENGTH) {
                        editText.setError(getString(R.string.passwordError));
                    }else {
                        editText.setError(null);
                    }
                }
            }
        });
    }

    public void setOnClickListener(Button button) {

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText mailText = (EditText) findViewById(R.id.mailTextBox);
                EditText passText = (EditText) findViewById((R.id.passwordTextBox));
                String mail = mailText.getText().toString().trim();
                String password = passText.getText().toString().trim();
                if(checkAccess(mail, password)) {
                    if (mail.equals(getString(R.string.adminMail)) && password.equals(getString(R.string.adminPassword))){
                        mailText.setText("");
                        passText.setText("");
                        load();
                        Intent intent = new Intent(getApplicationContext(), Menu.class);
                        startActivity(intent);
                    }else{showToast(getString(R.string.authenticationWrong));
                    }
                }
            }
        });
    }
    public boolean checkAccess(String mail, String password) {
        if(mail.isEmpty() || password.isEmpty()) {
            showToast(getString(R.string.fillalltheblanks));
            return false;
        }else {
            if(!checkmail(mail)) {
                showToast(getString(R.string.incorrectemail));
                return false;
            }
            if(password.length() < 6) {
                showToast(getString(R.string.incorrectpasswordformat));
                return false;
            }
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            finish();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, R.string.clickbackagain, Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, EXIT_INTERVAL_TIME);
    }
}


