package lasalleurl.sallesocialnetwork.Application;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.content.Intent;

import java.util.Calendar;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import lasalleurl.sallesocialnetwork.Adapters.NothingSelectedSpinnerAdapter;
import lasalleurl.sallesocialnetwork.R;
import lasalleurl.sallesocialnetwork.Model.User;

public class CreateUser extends BaseActivity implements DatePickerDialog.OnDateSetListener {

//    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_user);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        EditText date = (EditText) findViewById(R.id.bornDate);
        Spinner spinner = (Spinner) findViewById(R.id.genderSpinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.gender_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setPrompt(getString(R.string.genderText));
        spinner.setAdapter(new NothingSelectedSpinnerAdapter(adapter,R.layout.spinner_nothing_selected,this));

        Button button = (Button) findViewById(R.id.registerButton);
        setOnClickListener(button, spinner);
    }

    public boolean onOptionsItemSelected(MenuItem item){
        Intent myIntent = new Intent(getApplicationContext(), Menu.class);
        startActivityForResult(myIntent, 0);
        return true;
    }
    public void setOnClickListener(Button button, final Spinner spinner) {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
        EditText mailText = (EditText)findViewById(R.id.mailTextUser);
        EditText nameText = (EditText)findViewById(R.id.nameText);
        EditText surnameText = (EditText)findViewById(R.id.surnameText);
        Spinner spinnerValue = (Spinner)findViewById(R.id.genderSpinner);
        EditText bornDateText = (EditText) findViewById(R.id.bornDate);
        EditText studiesText = (EditText) findViewById(R.id.studies);
        String mail = mailText.getText().toString().trim();
        String name = nameText.getText().toString().trim();
        String surname = surnameText.getText().toString().trim();
        String bornDate = bornDateText.getText().toString().trim();
        String studies = studiesText.getText().toString().trim();

        if(!allFieldsFilled(mail, name, surname, spinnerValue, bornDate, studies)) {
            showToast(getString(R.string.fillalltheblanks));
        }else {
            if(!checkmail(mail)) {
                showToast(getString(R.string.incorrectemail));
            }else {
                if(existsMail(mail)){
                    showToast(getString(R.string.mailExists));
                }else {
                    String sex = spinnerValue.getSelectedItem().toString().trim();
                    //Hay que mirar el tema de la fecha yo lo puedo mirar el lunes
                    User user = new User(mail, name, surname, sex, bornDate, studies);
//                        Log.d("Usuario", user.toString());
                    addUser(user);
                    //Hay que añadirlo al arrayList
                    showToast(getString(R.string.correctUser));
//                        Toast.makeText(getApplicationContext(), user.toString(), Toast.LENGTH_SHORT).show();
                    Intent list = new Intent(getApplicationContext(), ListUsers.class);
                    startActivity(list);
                    finish();
                }
            }
        }
            }
        });
    }

    public boolean allFieldsFilled(String mail, String name, String surname, Spinner gender, String bornDate, String studies) {
        if(!mail.isEmpty() && !name.isEmpty() && !surname.isEmpty() && gender.getSelectedItem() != null && !bornDate.isEmpty() && !studies.isEmpty()) {
            return true;
        }
        return false;
    }

    public void DatePicker(View view) {
        DatePickerFragment fragment = new DatePickerFragment();
        fragment.show(getSupportFragmentManager(), "date");
    }
//    public void setDate(final Calendar calendar) {
    public void setDate(int year, int month, int day) {

//        final DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM);
        String date = Integer.toString(day) + "/" + Integer.toString(month) + "/" + Integer.toString(year);
        ((EditText) findViewById(R.id.bornDate)).setText(date);
//        ((EditText) findViewById(R.id.bornDate)).setText(dateFormat.format(calendar.getTime()));
//        ((EditText) findViewById(R.id.bornDate)).setText(day_name);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {

//        Calendar cal = new GregorianCalendar(year, month, day);
        setDate(year, month, day);

    }

    public static class DatePickerFragment extends DialogFragment {
        @Override
        public Dialog onCreateDialog(Bundle savedInstance) {
            final Calendar c;

                c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int day = c.get(Calendar.DAY_OF_MONTH);

                return new DatePickerDialog(getActivity(), (OnDateSetListener)getActivity(),
                        year, month, day);

        }
    }

}
