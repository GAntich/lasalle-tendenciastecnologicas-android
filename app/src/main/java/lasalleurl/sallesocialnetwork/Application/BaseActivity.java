package lasalleurl.sallesocialnetwork.Application;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lasalleurl.sallesocialnetwork.Model.User;


public class BaseActivity extends AppCompatActivity {

//GUILLE hay que controlar que no se introduzcan usuarios repetidos!!
    //Hay que conseguir hacer el load correctamente
    //Hay que dejar bonito el menu

    public MyApplication getMyApplication() {
        return ((MyApplication)getApplication());
    }

    public void addUser(User user) {
        MyApplication myApp = (MyApplication)getApplication();
        myApp.users.add(user);
    }
    public void showToast(String text) {
        Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
    }
    public boolean checkmail(String email)    {
        Pattern pattern = Pattern.compile(".+@.+\\.[a-z]+");
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
    public boolean existsMail(String mail){
        MyApplication myApp = (MyApplication)getApplication();
        for(User u : myApp.users){
            if(u.mail.equals(mail))return true;
        }
        return false;
    }
    public void save() {
        MyApplication myApp = (MyApplication)getApplication();
        JSONArray users = new JSONArray();
        try {
            for(User u: myApp.users) {
                JSONObject user = new JSONObject();
                user.put("mail", u.mail);
                user.put("name", u.name);
                user.put("lastname", u.lastname);
                user.put("sex", u.sex);
                user.put("birthdateString", u.birthdateString);
                user.put("age", u.age);
                user.put("studies", u.studies);
                user.put("isFriend", u.isFriend);

                users.put(user);
            }
            String jsonString = users.toString();
            FileOutputStream fos = getApplicationContext().openFileOutput("Data.txt", Context.MODE_PRIVATE);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(jsonString);
            os.close();
            fos.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public void load() {
        MyApplication myApp = (MyApplication)getApplication();
        try {
            FileInputStream fis = getApplicationContext().openFileInput("Data.txt");
            ObjectInputStream is = new ObjectInputStream(fis);
            String input = is.readObject().toString();
            is.close();
            fis.close();
            Type listType = new TypeToken<List<User>>() {}.getType();
            List<User> u = new Gson().fromJson(input, listType);
            myApp.users = u;


        }catch (Exception e) {
            e.printStackTrace();
        }


    }
}
