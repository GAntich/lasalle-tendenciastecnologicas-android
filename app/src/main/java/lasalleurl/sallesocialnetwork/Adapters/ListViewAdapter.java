package lasalleurl.sallesocialnetwork.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import lasalleurl.sallesocialnetwork.Application.MyApplication;
import lasalleurl.sallesocialnetwork.Application.ViewUser;
import lasalleurl.sallesocialnetwork.Model.User;
import lasalleurl.sallesocialnetwork.R;

/**
 * Created by Napsteir on 29/03/2017.
 */

public class ListViewAdapter extends ArrayAdapter<User> {
    private Context ctx;
    public ListViewAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<User> objects) {
        super(context, resource, objects);
        this.ctx = context;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View row = inflater.inflate(R.layout.user_row,parent,false);

        TextView nameCredentials = (TextView) row.findViewById(R.id.nameandlastname);
        TextView ageCredential = (TextView) row.findViewById(R.id.age);
        ImageView img_isFriend = (ImageView) row.findViewById(R.id.isFriend);

        final User u = getItem(position);


        Button viewButton = (Button) row.findViewById(R.id.buttonId);
        viewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent view = new Intent(getContext(), ViewUser.class);
                view.putExtra(ViewUser.EXTRA_USER, u);
                view.putExtra(ViewUser.EXTRA_POSITION, position);
                //Se ha añadido el flag por incompatibilidades con la api 23
                view.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ctx.startActivity(view);
            }
        });


        nameCredentials.setText(u.name+" "+u.lastname);
        ageCredential.setText(u.age);
        MyApplication myapp = (MyApplication)getContext();
        if(myapp.users.get(position).isFriend) {
            img_isFriend.setVisibility(View.VISIBLE);
        }else{
            img_isFriend.setVisibility(View.INVISIBLE);
        }

        return row;
    }
}
