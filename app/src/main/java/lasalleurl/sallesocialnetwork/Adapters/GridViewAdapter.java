package lasalleurl.sallesocialnetwork.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import lasalleurl.sallesocialnetwork.R;

/**
 * Created by Napsteir on 29/03/2017.
 */

public class GridViewAdapter extends BaseAdapter {
    /* ATTRIBUTES */
    Context mContext;
    private Integer[] mThumbIds = {
            R.drawable.ic_menu_invite, R.drawable.ic_menu_allfriends,R.drawable.ic_menu_revert
    };
    private Integer[] mStringIds = {
            R.string.createUser,R.string.listUsers,R.string.exit
    };
    public GridViewAdapter(Context ctx){
        this.mContext=ctx;
    }
    @Override
    public int getCount() {
        return mThumbIds.length;
    }
    @Override
    public Object getItem(int position) {
        return null;
    }
    @Override
    public long getItemId(int position) {
        return 0;
    }
    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.griditem, viewGroup, false);
        }

        ImageView imagen_grid = (ImageView) view.findViewById(R.id.imagen_grid);
        TextView string_grid = (TextView) view.findViewById(R.id.string_grid);

        imagen_grid.setImageResource(mThumbIds[position]);
        string_grid.setText(mStringIds[position]);

        return view;
    }
}
